const { Pool } = require('pg'); // postGre sql module
const dotenv = require('dotenv').config(); // Config local env fields

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
});


module.exports = pool; // Create a pool of db connections