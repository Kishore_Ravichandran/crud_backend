const winston = require('winston');
const winstonDailyRotateFile = require('winston-daily-rotate-file');
// Logging Tool to log values
const logFormat = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf(
        info => `${info.timestamp} ${info.level}: ${info.message}`,
    )
)

// Custom Logger so that the log files get generated every day
// generate logs folder with the dailay log files dynamically

const logger = winston.loggers.add('customLogger',{
    format : logFormat,
    transports : [
        new winstonDailyRotateFile({
            filename : './logs/custom-%DATE%.log',
            datePattern : 'YYYY-DD-MM',
            level : 'info'
        }),
        new winston.transports.Console({
            level : 'info'
        })
    ]
});



module.exports = logger;