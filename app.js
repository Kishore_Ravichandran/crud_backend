const express = require('express'); 

const bodyParser = require('body-parser');

const db = require('./util/db');
const winston = require('winston');  // Logging tool to generate log files

const multer = require('multer');
const router = express.Router();

const users = require('./routes/user');

const log = require('./util/logging');

// Add multer-middleware for file access
const fileStorage = multer.diskStorage({
  destination: (req,file,callback) => {
    callback(null,'uploads')
  },
  filename: (req,file,callback) => {
    callback(null,new Date().toISOString() + '-' + file.originalname);
  }
});

const fileFilter = (req,file,cb) => {
  if(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
    cb(null,true);
  } else 
  {
    cb(null,false);
  }
}



const app = express();
const PORT = 3000;
app.use(bodyParser.json()) // bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(multer({storage : fileStorage, fileFilter: fileFilter}).single('image'));






    

app.use('/api/vi/user', users.routes);  // route the user based on the API path

const server = app.listen(PORT);

log.info('Server Started on Port' + PORT);