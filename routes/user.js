const express = require('express');
const path = require('path');
const rootDir = require('../util/path');
const router = express.Router();
const userCtrl = require('../controllers/users');

// Routes fields

router.get('/list',userCtrl.list);
router.post('/add',userCtrl.add);
router.post('/update',userCtrl.update);
router.get('/delete/:mail',userCtrl.delete);

exports.routes = router;