const util = require('util');
const connection = require('../util/db');
var mysql   = require("mysql2");
const queries = require('../scripts/dbscripts.json'); // Get queries from Json file
const log = require('../util/logging');

// List the total number of users

exports.list = (req,res) => {
    var query = queries.select;
    connection.query(query, (err, result) => {
        if (err) {
            log.info('User Select' + err);
            res.status(500).send('Server error!' + err);
        } else {
            log.info('User Select Success' + result.rows);
            res.status(200).json(result.rows);
        }
    })
}

// Add users to the db
exports.add = (req,res) => {
    log.info('In Register');
    const image = req.file;
    const imageURL = image.path;
    var query = queries.add;
    var updateValues = [req.body.name,req.body.email,req.body.mobile,req.body.doj,req.body.job,req.body.location,req.body.countrycode];
    connection.query(query,updateValues,(err,result) => {
        if(err) {
            log.info('Error while creating' + err);
            res.status(500).send('Server error!' + err);
        } else {
            res.status(200).json({ user: 'User Created Successfully' });
        }
    })
}

// Delete the user using the mail Id

exports.delete = (req,res) => {
    log.info('In delete');
    var query = queries.delete;
    var updateValues = [req.params.mail];
    connection.query(query,updateValues,(err,result) => {
        if(err) {
            log.info('Error while deleting' + err);
            res.status(500).send('Server error!' + err);
        }else {
            res.status(200).json({ user: 'User Deleted Successfully' });
        }
    })
}

// Update the user based on mailId
exports.update = (req,res) => {
    log.info('In Update');
    var query = queries.update;
    var updateValues = [req.body.name,req.body.email,req.body.mobile,req.body.doj,req.body.job,req.body.location,req.body.countrycode,req.body.email];
    connection.query(query,updateValues,(err,result) => {
        if(err) {
            log.info('Error while updating' + err);
            res.status(500).send('Server error!' + err);
        } else {
            res.status(200).json({ user: 'User Updated Successfully' });
        }
    })
}